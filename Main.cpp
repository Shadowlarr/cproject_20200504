#include <iostream>

struct Bag
{
	std::string Books[];
};



struct Student
{
	int Age = 0;
	int Height = 0;
	std::string Name = 0;
	Bag* MyBag = nullptr;

	void GetInfo()
	{
		std::cout << "Student struct" << '\n';
	}
};


int main()
{
	Student* ptr = new Student{ 10,160, "Paul" };
	ptr->GetInfo();
	return 0;
}